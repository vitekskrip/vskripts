<?php include $_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'; ?>
<?php
if (!isset($_SESSION['isAuth'])) {
    header('Location:/login/'); 
}
$user = getInfoAboutUserBySession($_SESSION['id']);
$groups = getUserGroups($_SESSION['id']);
?>

<main class="flex-1 container mx-auto bg-white overflow-hidden px-4 sm:px-6">
    <div class="py-4 pb-8 space-y-2">
        <h1 class="text-black text-3xl font-bold mb-4">Личный кабинет</h1>
        
        <div class="space-y-2">
            <div class="space-y-2 pb-4 border-b">
                <p class="text-xl">Мои профиль</p>

                <div class="flex max-w-xl">
                    <div class="flex-1 border px-4 py-2 bg-gray-200 font-bold">ФИО</div>
                    <div class="flex-1 border px-4 py-2"><?=$user['name'] ?></div>
                </div>
                <div class="flex max-w-xl">
                    <div class="flex-1 border px-4 py-2 bg-gray-200 font-bold">Email</div>
                    <div class="flex-1 border px-4 py-2"><?=$user['email'] ?></div>
                </div>
                <div class="flex max-w-xl">
                    <div class="flex-1 border px-4 py-2 bg-gray-200 font-bold">Телефон</div>
                    <div class="flex-1 border px-4 py-2"><?=$user['phone_number'] ?></div>
                </div>
                <div class="flex max-w-xl">
                    <div class="flex-1 border px-4 py-2 bg-gray-200 font-bold">Активность</div>
                    <div class="flex-1 border px-4 py-2"><?=$user['is_active'] ? 'Да' : 'Нет' ?></div>
                </div>
                <div class="flex max-w-xl">
                    <div class="flex-1 border px-4 py-2 bg-gray-200 font-bold">Подписан на рассылку</div>
                    <div class="flex-1 border px-4 py-2"><?=$user['is_agree'] ?? 'Не указано' ?></div>
                </div>
            </div>
        </div>
        
        <?php if ($groups) { ?>
        <div class="space-y-2">
            <p class="text-xl">Мои группы</p>

            <ul class="list-inside list-disc">
                <?php foreach ($groups as $group) { ?>
                <li><span class="font-bold"><?=$group['name'] ?></span> - <?=$group['description'] ?></li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>            
    </div>
</main>
<?php includeTemplate('footer.php'); ?>