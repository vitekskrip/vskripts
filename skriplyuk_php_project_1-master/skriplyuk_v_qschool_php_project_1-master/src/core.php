<?php
session_start();
include 'includeTemplate.php';
include 'arrSort.php';
include 'cutStr.php';
include 'getMenu.php';
include 'checkData.php';
include 'getCars.php';
include 'isCurrentPage.php';
include 'db/getInfoAboutUser.php';
include 'db/dbConnect.php';
include 'db/insertRegUser.php';
include 'auth/logIn.php';
include 'auth/signUp.php';
include 'auth/extendCookie.php';
