<?php

$errorReg = false;
$passwordConfirm = false;
$passwordLengthConfirm = false;

if (! empty($_POST) && $_SERVER['REQUEST_URI'] === '/register/') {
    if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['password']) || empty($_POST['password_confirmation'])) {
        $errorReg = true;
    } elseif ($_POST['password'] == $_POST['password_confirmation'] ) {
        $passwordConfirm = true;
        if (strlen($_POST['password']) >= 6) {
            $passwordLengthConfirm = true;
            insertUserIntoDb();
            header('Location: /login/ ');
        }
    }
} else {
    $errorReg=true;
}
