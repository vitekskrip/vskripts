<?php

if (!currentPage('/login/') && !currentPage('/register/')) {
    if (isset($_SESSION['isAuth']) && isset($_COOKIE['email'])) {
        setcookie('email', $_COOKIE['email'], time() + 60 * 60 * 24 * 32, '/');
    }
}