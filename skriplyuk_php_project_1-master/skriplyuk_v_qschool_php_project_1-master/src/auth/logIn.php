<?php
include $_SERVER['DOCUMENT_ROOT'] . '/src/auth/setSeCo.php';

$success = false;
$errorAuth = false;
$accessDenied = false;

if (! empty($_POST) && $_SERVER['REQUEST_URI'] === '/login/') {
    if (empty($_POST['email']) || empty($_POST['password'])) {
        $errorAuth = true;
    } else {
        $user = checkUserByEmailAndPassword($_POST['email'], $_POST['password']);
        if ($user && $user['is_active']) {
            $success = true;
            $user['is_agree'] ? $user['is_agree'] = 'Да' : $user['is_agree'] = 'Нет';
            setSessionAndCoo($user);
        } elseif (!$user) {
            $errorAuth = true;
        } else {
            $accessDenied = true;
        }
    }
}
