<?php

function setSessionAndCoo($user)
{
    $_SESSION['isAuth'] = true;
    $_SESSION['email'] = $user['email'];
    $_SESSION['id'] = $user['id'];
    setcookie('email', $user['email'], time() + 60 * 60 * 24 * 32, '/');

    header('Location: /');
}
