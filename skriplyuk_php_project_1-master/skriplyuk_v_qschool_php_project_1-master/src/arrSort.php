<?php

function arraySort(array $array, $key = 'sort', $sort = SORT_ASC): array
{
    usort($array, function ($item1, $item2) use ($key, $sort) {
        return $sort === SORT_ASC ? $item1[$key] <=> $item2[$key] : $item2[$key] <=> $item1[$key];
    });
    return $array;
}
