<?php

function cutString($line, $length, $more, $appends = '...'): string
{
    if (mb_strlen($line) > $length) {
        $line = mb_substr($line, 0, $more) . $appends;
    }
    return $line;
}
