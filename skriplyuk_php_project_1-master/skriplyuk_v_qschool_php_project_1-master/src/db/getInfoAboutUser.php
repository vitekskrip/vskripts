<?php 

function checkUserByEmailAndPassword($email, $password)
{
    $connection = dbConnect();
    $emailForm = mysqli_real_escape_string($connection, $email);
    $passForm = mysqli_real_escape_string($connection, $password);
    $user = mysqli_fetch_assoc(mysqli_query($connection, "SELECT * FROM users WHERE email = '$emailForm' LIMIT 1")); 
    return ($user && password_verify($passForm, $user['password'])) ? $user : null; 
}

function getUserGroups($id)
{
    $connection = dbConnect();
    $id = mysqli_real_escape_string($connection, $id);
    $query = mysqli_query($connection, "SELECT g.name, g.description FROM groups g JOIN group_user gu on g.id = gu.groups_id JOIN users u on gu.users_id = u.id WHERE u.id = '$id' ");
    return mysqli_fetch_all($query, MYSQLI_ASSOC);
}

function getInfoAboutUserBySession($id)
{
    $connection = dbConnect();
    $id = mysqli_real_escape_string($connection, $id);
    return mysqli_fetch_assoc(mysqli_query($connection, "SELECT * FROM users WHERE id = '$id' LIMIT 1"));
}