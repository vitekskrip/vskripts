<?php

function dbConnect()
{
    static $connect = null;

    if (null === $connect) {
        $connect = mysqli_connect('newgrade.vpool', 'test', 'test', 'skriplyuk_v_qschool_test') or die('connection Error');
    }

    return $connect;
}