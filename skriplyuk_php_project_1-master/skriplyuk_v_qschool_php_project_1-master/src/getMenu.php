<?php

function getMenu()
{
    $menu = [
        [
            'title' => 'Главная страница',
            'path' => '/',
            'sort' => 0,
            'isBasic' => true,
        ],
        [
            'title' => 'Популярное',
            'path' => '/popular/',
            'sort' => 1,
            'isBasic' => true,
        ],
        [
            'title' => 'О нас',
            'path' => '/aboutUs/',
            'sort' => 3,
            'isBasic' => true,
        ],
        [
            'title' => 'О QSOFT',
            'path' => '/aboutQsoft/',
            'sort' => 4,
            'isBasic' => true,
        ],
        [
            'title' => 'Каталог',
            'path' => '/catalog/',
            'sort' => 2,
            'isBasic' => false,
        ],
    ];

    foreach ($menu as $key => $line) {
        if (isset($_SESSION['isAuth']) || $line['isBasic']) { 
            $menu[$key]['title'] = cutString($line['title'], 15, 12); 
        } else {
            unset($menu[$key]);
        }   
    }
    $menu = arraySort($menu);

    return $menu;
}
