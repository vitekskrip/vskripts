<?php

require $_SERVER['DOCUMENT_ROOT'] . '/oop/level1/task1/Cup/Cup.php';
require $_SERVER['DOCUMENT_ROOT'] . '/oop/level1/task1/Table/Table.php';
require $_SERVER['DOCUMENT_ROOT'] . '/oop/level1/task1/Fork/Fork.php';

use \Fork\Fork as Fork;
use \Cup\Cup as Cup;
use \Table\Table as Table;

$fork1 = new Fork(3);
$fork2 = new Fork(4);

$cup1 = new Cup(200);
$cup2 = new Cup(400);
$cup2 = new Cup(1000);

$table1 = new Table('white');

// хороший тон - объявить все свойства класса заранее вверху класса
