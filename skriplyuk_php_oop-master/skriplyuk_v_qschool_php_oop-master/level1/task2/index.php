<?php

require $_SERVER['DOCUMENT_ROOT'] . '/oop/level1/task2/PowerfulComputer/PowerfulComputer.php';

use \PowerfulComputer\PowerfulComputer as PowerPC;

$superPc1 = new PowerPC(512, 2.7, 'vscode.exe');
$superPc2 = new PowerPC(1024, 3.2, 'csgo.exe');

$superPc1->executeProgram('vscode.exe');
$superPc1->executeProgram('csgo.exe');
$superPc1->executeProgram('edge.exe');
$superPc1->executeProgram('opera.exe');
$superPc1->executeProgram('minecraft.exe');

$superPc2->executeProgram('vscode.exe');
$superPc2->executeProgram('csgo.exe');
$superPc2->executeProgram('edge.exe');
$superPc2->executeProgram('opera.exe');
$superPc2->executeProgram('minecraft.exe');
