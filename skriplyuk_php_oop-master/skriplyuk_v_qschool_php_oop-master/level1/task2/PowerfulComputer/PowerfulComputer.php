<?php

namespace PowerfulComputer;

class PowerfulComputer
{
    public $memorySize;
    public $processorClockSpeed;
    public $favoriteProgram;

    public function __construct($memorySize, $processorClockSpeed, $favoriteProgram)
    {
        $this->memorySize = $memorySize;
        $this->processorClockSpeed = $processorClockSpeed;
        $this->favoriteProgram = $favoriteProgram;
    }

    public function executeProgram($program)
    {
        $program == $this->favoriteProgram ? print("Устройство $this->memorySize GB и $this->processorClockSpeed GHz выполнил программу: \"$program\" и завис на долгое время...  </br>") : print("Устройство $this->memorySize GB и $this->processorClockSpeed GHz выполнил программу: \"$program\" </br>");
    }
}
