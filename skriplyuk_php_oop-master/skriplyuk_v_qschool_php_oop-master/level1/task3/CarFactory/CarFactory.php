<?php

namespace CarFactory;

use Car\Car as Car;

class CarFactory
{
    private Car $car;

    public function createCar($name)
    {
        $price = rand(50000, 1000000);
        return new Car($name, $price);
    }
}
