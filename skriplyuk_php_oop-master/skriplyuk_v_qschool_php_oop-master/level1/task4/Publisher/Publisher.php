<?php

namespace Publisher;

use CensorService\CensorService as CensorService;
use Article\Article as Article;

class Publisher
{
    private CensorService $censor;

    public function __construct(CensorService $censor)
    {
        $this->censor = $censor;
    }

    private function send($name, $content, $chanel)
    {
        print("Статья: $name была опубликована в $chanel. $content </br>");
    }

    public function publish(Article $article)
    {
        $name = $article->name;
        $ageCategory = $article->ageCategory;
        $content = $ageCategory < 18 ? $this->censor->censor($article->content) : $article->content;
        $article->isContainImg ? Publisher::send($name, $content, "Instagram") : null;
        Publisher::send($article->name, $content, "Интернет-блог");
    }
}
