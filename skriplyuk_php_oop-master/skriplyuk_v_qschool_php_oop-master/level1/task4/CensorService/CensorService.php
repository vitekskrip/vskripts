<?php

namespace CensorService;

class CensorService
{
    public function censor(string $text)
    {
        return "</br><b>Внимание! Уберите от экрана детей! Статья содержит неприемлемые для них материалы!</b> " . $text;
    }
}
