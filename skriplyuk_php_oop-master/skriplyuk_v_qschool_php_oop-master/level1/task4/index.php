<?php
include $_SERVER['DOCUMENT_ROOT'] . '/oop/level1/task4/Article/Article.php';
include $_SERVER['DOCUMENT_ROOT'] . '/oop/level1/task4/Publisher/Publisher.php';
include $_SERVER['DOCUMENT_ROOT'] . '/oop/level1/task4/CensorService/CensorService.php';

use Publisher\Publisher as Publisher;
use Article\Article as Article;
use CensorService\CensorService as CensorService;

$publisher = new Publisher($censor = new CensorService);
$article1 = new Article('Статья 1', 'Контент статьи 1');
$article2 = new Article('Статья 2', 'Контент статьи 2', 20);
$article3 = new Article('Статья 3', 'Контент статьи 3', null , true);

$publisher->publish($article1);
echo "</br>";
$publisher->publish($article2);
echo "</br>";
$publisher->publish($article3);
echo "</br>";
