<?php

namespace Article;

class Article
{
    public $name;
    public $content;
    public $ageCategory;
    public $isContainImg;

    public function __construct($name, $content, $ageCategory = 17, $isContainImg = null)
    {
        $this->name = $name;
        $this->content = $content;
        $this->ageCategory = $ageCategory;
        $this->isContainImg = $isContainImg;
    }
}
