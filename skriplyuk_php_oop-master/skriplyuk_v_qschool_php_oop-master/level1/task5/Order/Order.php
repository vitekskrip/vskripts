<?php

namespace Order;

use Basket\Basket as Basket;

class Order
{
    private $basket;

    public function __construct(Basket $basket)
    {
        $this->basket = $basket;
    }

    public function getPrice()
    {
        $orderPrice = $this->getBasket()->getPrice() + 300;
        return $orderPrice;
    }
    
    public function getBasket()
    {
        return $this->basket;
    }
}
