<?php

include $_SERVER['DOCUMENT_ROOT'] . '/oop/level1/task5/Basket/Basket.php';
include $_SERVER['DOCUMENT_ROOT'] . '/oop/level1/task5/BasketPosition/BasketPosition.php';
include $_SERVER['DOCUMENT_ROOT'] . '/oop/level1/task5/Order/Order.php';
include $_SERVER['DOCUMENT_ROOT'] . '/oop/level1/task5/Product/Product.php';

use Order\Order as Order;
use Basket\Basket as Basket;
use BasketPosition\BasketPosition as BasketPosition;
use Product\Product as Product;

$product1 = new Product("Рубашка", 100);
$product2 = new Product("Футболка", 200);
$product3 = new Product("Джинсы", 500);
$product4 = new Product("Кайф", 10000);
$basketPos1 = new BasketPosition($product1, 10);
$basketPos2 = new BasketPosition($product2, 5);
$basketPos3 = new BasketPosition($product3, 2);
$basket = new Basket([$basketPos1, $basketPos2, $basketPos3]);
$order1 = new Order($basket);

print("Создан заказ, на общую сумму: " . $order1->getPrice() . ". Состав заказа: " . $order1->getBasket()->describe());

echo "</br>";
echo "</br>";
$basket->addProduct($product4, 4);
$order2 = new Order($basket);

print("Создан заказ, на общую сумму: " . $order2->getPrice() . ". Состав заказа: " . $order2->getBasket()->describe());
