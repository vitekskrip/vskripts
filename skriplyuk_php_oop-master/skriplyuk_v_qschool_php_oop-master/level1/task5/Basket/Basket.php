<?php

namespace Basket;

use \Product\Product as Product;
use \BasketPosition\BasketPosition as BasketPosition;

class Basket
{
    public $structure;

    public function __construct($structure = [])
    {
        $this->structure = $structure;
    }

    public function addProduct(Product $product, $quantity)
    {
        $this->structure[] = new BasketPosition($product, $quantity);
    }

    public function getPrice()
    {
        $totalPrice = 0;
        foreach ($this->structure as $basketPosition) {
            $totalPrice += $basketPosition->getPrice();
        }
        return $totalPrice;
    }

    public function describe()
    {
        foreach ($this->structure as $basketPosition) {
            print($basketPosition->getProduct()->getName() . ' - ' . $basketPosition->getProduct()->getPrice() . ' - ' . $basketPosition->getQuantity() . "</br>");
        }
    }
}
