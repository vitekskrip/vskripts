<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task2/Agent/Agent.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task2/SecretObject/SecretObject.php';

use Student\Student as Student;
use UnluckySpy\UnluckySpy;
use SecretAgent\SecretAgent as SecretAgent;
use Library\Library as Library;
use Area51\Area51 as Area51;

$agent1 = new Student();
$agent2 = new SecretAgent();
$agent3 = new UnluckySpy();

$library = new Library();
$zone51 = new Area51();

$agent1->getSecretInformation($library);
echo "</br>";
$agent1->getSecretInformation($zone51);
echo "</br>";
echo "</br>";
$agent2->getSecretInformation($library);
echo "</br>";
$agent2->getSecretInformation($zone51);
echo "</br>";
echo "</br>";
$agent3->getSecretInformation($library);
echo "</br>";
$agent3->getSecretInformation($zone51);
