<?php

namespace Student;

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task2/Agent/Agent.php';

use Agent\Agent as Agent;

class Student extends Agent
{
    public function getAccessLevel()
    {
        return 1;
    }
}
