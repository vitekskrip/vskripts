<?php

namespace SecretAgent;

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task2/Agent/Agent.php';

use Agent\Agent as Agent;

class SecretAgent extends Agent
{
    public function getAccessLevel()
    {
        return 5;
    }
}
