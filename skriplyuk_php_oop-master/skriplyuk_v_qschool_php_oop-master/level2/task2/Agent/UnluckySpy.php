<?php

namespace UnluckySpy;

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task2/Agent/Agent.php';

use Agent\Agent as Agent;

class UnluckySpy extends Agent
{
    public function getAccessLevel()
    {
        return rand(0, 6);
    }
}
