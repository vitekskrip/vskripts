<?php

namespace Agent;

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task2/SecretObject/SecretObject.php';

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task2/Agent/SecretAgent.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task2/Agent/Student.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task2/Agent/UnluckySpy.php';

use SecretObject\SecretObject as SecretObject;

abstract class Agent
{
    abstract public function getAccessLevel();

    public function getSecretInformation(SecretObject $secretObject)
    {
        print($secretObject->getSecretInformationForAgent($this->getAccessLevel()));
    }
}
