<?php

namespace Library;

use SecretObject\SecretObject as SecretObject;

class Library extends SecretObject
{
    private $secretInformation = 'Инопланетяне есть';

    protected function agentLevelHasAccess($agentAccessLevel)
    {
        return $agentAccessLevel >= 1 ? true : false;
    }

    protected function getSecretInformation()
    {
        return $this->secretInformation;
    }
}
