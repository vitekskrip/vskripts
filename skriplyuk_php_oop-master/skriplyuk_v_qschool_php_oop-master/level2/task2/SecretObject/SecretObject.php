<?php

namespace SecretObject;

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task2/SecretObject/Library.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task2/SecretObject/Area51.php';

abstract class SecretObject
{
    abstract protected function agentLevelHasAccess($agentAccessLevel);
    
    abstract protected function getSecretInformation();

    public function getSecretInformationForAgent($agentAccessLevel)
    {
        return $this->agentLevelHasAccess($agentAccessLevel) ? $this->getSecretInformation() : "Доступ запрещен";
    }
}
