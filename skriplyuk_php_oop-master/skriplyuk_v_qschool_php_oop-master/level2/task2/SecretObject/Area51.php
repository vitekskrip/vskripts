<?php

namespace Area51;

use SecretObject\SecretObject as SecretObject;

class Area51 extends SecretObject
{
    private $secretInformation = 'Инопланетян нет';

    protected function agentLevelHasAccess($agentAccessLevel)
    {
        return $agentAccessLevel >= 6 ? true : false;
    }

    protected function getSecretInformation()
    {
        return $this->secretInformation;
    }
}
