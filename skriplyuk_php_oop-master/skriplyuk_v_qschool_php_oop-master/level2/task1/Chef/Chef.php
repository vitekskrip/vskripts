<?php

namespace Chef;

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task1/Cook/Cook.php';


use Cook\Cook as Cook;
use Dish\Dish as Dish;

class Chef extends Cook
{
    protected function getPrice(Dish $dish): int
    {
        return $dish->getPrice() * 5;
    }
}
