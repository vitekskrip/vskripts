<?php

namespace Cook;

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task1/Dish/Dish.php';

use Dish\Dish as Dish;

class Cook
{
    protected $order = [];

    public function addDishToOrder(Dish $dish)
    {
        $this->order[] = $dish;
    }

    public function prepareFood()
    {
        $checkPrice = 0;
        $str = "Повар приготовил: ";
        foreach ($this->order as $dish) {
            print($str . $dish->getName() . "</br>");
            $checkPrice += $this->getPrice($dish);
        }
        print("Стоимость заказа: $checkPrice" . "</br>");
    }

    protected function getPrice(Dish $dish): int
    {
        return $dish->getPrice();
    }
}
