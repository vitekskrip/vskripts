<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task1/Chef/Chef.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task1/Cook/Cook.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task1/Dish/Pasta.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task1/Dish/Pizza.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task1/Dish/Cake.php';


use Pasta\Pasta as Pasta;
use Cake\Cake as Cake;
use Pizza\Pizza as Pizza;
use Cook\Cook as Cook;
use Chef\Chef as Chef;

$dish1 = new Pasta();
$dish2 = new Cake();
$dish3 = new Pizza();

$orderToCook = new Cook();
$orderToCook->addDishToOrder($dish1);
$orderToCook->addDishToOrder($dish2);
$orderToCook->addDishToOrder($dish3);
$orderToCook->prepareFood();

$orderToCook = new Chef();
$orderToCook->addDishToOrder($dish1);
$orderToCook->addDishToOrder($dish2);
$orderToCook->addDishToOrder($dish3);
$orderToCook->prepareFood();
