<?php

namespace Dish;

abstract class Dish
{
    abstract public function getName(): string;
    
    abstract public function getPrice(): int;
}
