<?php

namespace Cake;

use Dish\Dish as Dish;

class Cake extends Dish
{
    public function getName(): string
    {
        return 'Торт';
    }

    public function getPrice(): int
    {
        return 200;
    }
}
