<?php

namespace Pasta;

use Dish\Dish as Dish;

class Pasta extends Dish
{
    public function getName(): string
    {
        return 'Паста';
    }

    public function getPrice(): int
    {
        return 250;
    }
}
