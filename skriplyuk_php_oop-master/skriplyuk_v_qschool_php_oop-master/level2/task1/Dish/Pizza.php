<?php

namespace Pizza;

use Dish\Dish as Dish;

class Pizza extends Dish
{
    public function getName(): string
    {
        return 'Пицца';
    }

    public function getPrice(): int
    {
        return 600;
    }
}
