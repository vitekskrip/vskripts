<?php

include $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task3/Edible/Beef.php';
include $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task3/Edible/Flower.php';
include $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task3/Edible/Woods.php';
include $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task3/Edible/Piano.php';
include $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task3/Edible/Keyboard.php';

include $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task3/Omnivore/Omnivore.php';

use Keyboard\Keyboard as Keyboard;
use Piano\Piano as Piano;
use Beef\Beef as Beef;
use Woods\Woods as Woods;
use Flower\Flower as Flower;

use Omnivore\Omnivore as Omnivore;

$keyboard = new Keyboard();
$piano = new Piano();
$beef = new Beef();
$woods = new Woods();
$flower = new Flower();

$omnivore = new Omnivore();
$omnivore->eat($keyboard);
$omnivore->eat($piano);
$omnivore->eat($beef);
$omnivore->eat($woods);
$omnivore->eat($flower);
