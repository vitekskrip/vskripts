<?php

namespace Beef;

include $_SERVER['DOCUMENT_ROOT'] . '/oop/level2/task3/Edible/Edible.php';


use Edible\Edible as Edible;

class Beef implements Edible
{
    public function edible()
    {
        return true;
    }

    public function taste()
    {
        return 'что-то волшебное';
    }
}
