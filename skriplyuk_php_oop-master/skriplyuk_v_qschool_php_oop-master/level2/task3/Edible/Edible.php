<?php

namespace Edible;

interface Edible
{
    public function edible();
    
    public function taste();
}
