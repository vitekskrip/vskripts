<?php

namespace Woods;

use Edible\Edible as Edible;

class Woods implements Edible
{
    public function edible()
    {
        return false;
    }

    public function taste()
    {
        return "деревянный";
    }
}
