<?php

namespace Flower;

use Edible\Edible as Edible;

class Flower implements Edible
{
    public function edible()
    {
        return true;
    }

    public function taste()
    {
        return "растение";
    }
}
