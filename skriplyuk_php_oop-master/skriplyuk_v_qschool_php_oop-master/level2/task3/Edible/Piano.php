<?php

namespace Piano;

use Edible\Edible as Edible;

class Piano implements Edible
{
    public function edible()
    {
        return false;
    }

    public function taste()
    {
        return "что-то музыкальное";
    }
}
