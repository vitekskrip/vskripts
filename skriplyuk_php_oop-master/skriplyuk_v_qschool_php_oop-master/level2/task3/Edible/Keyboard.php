<?php

namespace Keyboard;

use Edible\Edible as Edible;

class Keyboard implements Edible
{
    public function edible()
    {
        return false;
    }

    public function taste()
    {
        return "пластмасса";
    }
}
