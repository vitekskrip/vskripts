<?php

namespace Omnivore;

use Edible\Edible as Edible;

class Omnivore
{
    public function eat(Edible $edible)
    {
        return $edible->edible() ? print("Очень вкусно, я съел: " . get_class($edible) . ". На вкус " . $edible->taste() . "</br>") : print("Даже я такое не ем: " . get_class($edible) . ". Фу." . "</br>");
    }
}
