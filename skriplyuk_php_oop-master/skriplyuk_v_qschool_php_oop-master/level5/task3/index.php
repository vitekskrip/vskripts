<?php

include __DIR__ . '/autoload.php';

use App\UserFormValidator\UserFormValidator;
use App\User\User;

$successValid = false;
$successSave = false;
if (! empty($_POST)) {
    try {
        $user = new User();
        $user->load($_POST['id']);
        $successValid = (new UserFormValidator())->validate($_POST);
        if (!$user->save($_POST)) {
            throw new Exception("Не удалось сохранивать в базу данных");
        }
        $successSave = true; 
    } catch (Exception $e) {
        $error = $e->getMessage();
    }
}
?>

<!doctype html>
<html class="antialiased" lang="ru">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
    <?php
    if ($successSave) {
        echo "Данные успешно сохранены в базу данных!";
    } elseif (isset($error)) {
        echo $error;
    }
    ?>
    <form method="POST">
        <div>
            <label for="id">id</label>
            <input name="id" value="" type="number">
        </div>
        <div>
            <label for="name">name</label>
            <input name="name" value="">
        </div>
        <div>
            <label for="age">age</label>
            <input name="age" value="" type="number">
        </div>
        <div>
            <label for="email">email</label>
            <input name="email" type="email" value="">
        </div>
        <button type="submit">
            Отправить
        </button>   
    </form>
</body>
</html>
