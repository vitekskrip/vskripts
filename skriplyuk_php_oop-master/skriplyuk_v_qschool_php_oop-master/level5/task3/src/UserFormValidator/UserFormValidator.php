<?php

namespace App\UserFormValidator;

use Exception;

class UserFormValidator
{
    public function validate($userInfo)
    {
        if (empty($userInfo['email']) && filter_var($userInfo['email'], FILTER_VALIDATE_EMAIL)) {
            throw new Exception('Поле email не должно быть пустым и соответствовать формату');
        }
        
        if (empty($userInfo['age']) || (int)$userInfo['age'] < 18) {
            throw new Exception('Поле age должно быть не пустым и больше 18');
        }

        if (empty($userInfo['name'])) {
            throw new Exception('Поле name не должно быть пустым');
        }

        return true;
    }
}
