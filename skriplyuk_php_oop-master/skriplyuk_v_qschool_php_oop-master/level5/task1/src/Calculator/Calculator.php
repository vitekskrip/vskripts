<?php

namespace App\Calculator;

class Calculator
{
    public static function calculate($number1, $number2, callable $callback)
    {
        echo 'Результат: ' . $callback($number1, $number2) . PHP_EOL;
    }
}
