<?php

include __DIR__ . '/autoload.php';

use App\Calculator\Calculator;
use App\MulAndDiv\MulAndDiv;

function add($num1, $num2)
{
    return $num1 + $num2;
}

$callbacks = [
    'add',

    function ($num1, $num2) {
        return $num1 - $num2;
    },

    [MulAndDiv::class, 'multiplication'],

    [new MulAndDiv(), 'division']
];

echo '<pre>';

for ($i = 1; $i <= 10; $i++) {
    echo "Пара чисел: $i и " . $i + 2 . PHP_EOL;
    foreach ($callbacks as $callback) {
        Calculator::calculate($i, $i + 2, $callback);
    }
}

echo '</pre>';
