<?php

spl_autoload_register(function ($class) {

    $prefix = 'App\\';
    $len = strlen($prefix);

    $baseDir = __DIR__ . '/src/';

    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relativeClass = substr($class, $len);
    $relativeClass = str_replace('\\', '/', $relativeClass);
    $relativePath = $baseDir . $relativeClass . '.php';

    if (file_exists($relativePath)) {
        require $relativePath;
    }
});
