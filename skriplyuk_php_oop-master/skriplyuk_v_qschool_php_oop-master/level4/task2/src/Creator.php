<?php

namespace App;

use App\Item\EmptyItem;

class Creator
{
    public static function create($name)
    {
        $name = __NAMESPACE__ . '\\' . ucfirst($name);
        return class_exists($name) ? new $name($name) : new EmptyItem($name);
    }
}
