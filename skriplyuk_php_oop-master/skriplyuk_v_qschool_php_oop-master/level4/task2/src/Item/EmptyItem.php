<?php

namespace App\Item;

use App\Item\Item;

class EmptyItem extends Item
{
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function show()
    {
        echo 'Класс ' . $this->name . ' не найден' . PHP_EOL;
    }
}
