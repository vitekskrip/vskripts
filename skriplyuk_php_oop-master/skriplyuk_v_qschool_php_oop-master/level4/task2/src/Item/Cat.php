<?php

namespace App\Item;

use App\Item\Item;

class Cat extends Item
{
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function __show()
    {
        echo 'Я ' . $this->name;
    }
}
