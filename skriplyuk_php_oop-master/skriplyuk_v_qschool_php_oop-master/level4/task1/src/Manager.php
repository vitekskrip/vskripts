<?php

namespace App;

use \App\Instrument\Instrument;
use \App\Papers\Papers;

class Manager
{
    public function place($item)
    {
        if ($item instanceof Papers) {
            $str = "Положил " . get_class($item) . " на стол" . PHP_EOL;
        } elseif ($item instanceof Instrument) {
            $str = "Положил " . get_class($item) . " внутрь стола" . PHP_EOL;
        } else {
            $st = is_object($item) ? get_class($item) : $item;
            $str = "Выкинул $st в корзину" . PHP_EOL;
        }
        return $str;
    }
}
