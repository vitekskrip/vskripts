<?php

require __DIR__ . '/autoload.php';

use \App\Apple\Apple;
use \App\Manager;
use \App\Instruments\Hammer;
use \App\Papers\Notebook;

$manager = new Manager();

$paper1 = new Notebook();

$instrument = new Hammer();
$number = 5;
$paper2 = new Notebook();
$apple = new Apple();

echo $manager->place($paper1);
echo $manager->place($instrument);
echo $manager->place($number);
echo $manager->place($paper2);
echo $manager->place($apple);
