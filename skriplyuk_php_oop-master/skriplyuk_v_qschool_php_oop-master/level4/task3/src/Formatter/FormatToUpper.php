<?php

namespace App\Formatter;

use App\Formatter\Formatter;

class FormatToUpper implements Formatter
{
    public function format($string)
    {
        return mb_strtoupper($string) . mb_strtolower($string);
    }
}
