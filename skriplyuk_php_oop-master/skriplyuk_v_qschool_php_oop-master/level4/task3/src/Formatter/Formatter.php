<?php

namespace App\Formatter;

interface Formatter
{
    public function format($string);
}
