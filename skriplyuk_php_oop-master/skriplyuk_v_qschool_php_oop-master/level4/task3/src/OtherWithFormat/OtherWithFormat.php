<?php

namespace App\OtherWithFormat;

class OtherWithFormat
{
    public function format($string)
    {
        return mb_strtoupper("осторожно, это другое форматирование ") . $string;
    }
}
