<?php

namespace App\Renderable;

use App\Renderable\Renderable;

class OutputForBrowser implements Renderable
{
    public function render($string)
    {
        echo "<font color=\"blue\" size=\"14\">$string</font>";
    }
}
