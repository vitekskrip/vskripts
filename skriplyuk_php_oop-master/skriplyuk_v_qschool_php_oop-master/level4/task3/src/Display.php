<?php

namespace App;

use App\Formatter\Formatter;
use App\Renderable\Renderable;

class Display
{
    public static function show($formatter, $string)
    {
        if ($formatter instanceof Renderable) {
            $formatter->render($string);
        } elseif ($formatter instanceof Formatter || method_exists($formatter, 'format')) {
            echo $formatter->format($string);
        } else {
            echo $string;
        }
    }
}
