<?php

require __DIR__ . '/autoload.php';

use App\Formatter\FormatToUpper;
use App\Display;
use App\OtherWithFormat\OtherWithFormat;
use App\OtherWithoutFormat\OtherWithoutFormat;
use App\Renderable;
use App\Renderable\OutputForBrowser;

$strs = [
    'Тестовая строка 1',
    'Тестовая строка 2',
    'Еще одна строка',
    'Можно еще подлиннее фывафыввфывфывячсвфы'
];

$objects = [
    new FormatToUpper(), // вывод строки в верхнем регистре и за ней в нижнем
    new OutputForBrowser(), // синяя строка
    new OtherWithFormat(), // с format() - вначало добавлено предупреждение
    new OtherWithoutFormat() // без format()
];

$display = new Display();

?>
<pre>
<?php
foreach ($objects as $obj) {
    foreach ($strs as $str) {
        ?>
        <pre>
        <?php
        $display->show($obj, $str);
        ?>
        </pre>
        <?php
    }
}



