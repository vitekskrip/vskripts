<?php

namespace ArrayReader;

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level3/task3/Drivers/Reader.php';

use Reader\Reader as Reader;

class ArrayReader implements Reader
{
    private $massive = ['главная', 'каталог', 'о нас', 'о qSoft'];

    public function read()
    {
        return $this->massive;
    }
}
