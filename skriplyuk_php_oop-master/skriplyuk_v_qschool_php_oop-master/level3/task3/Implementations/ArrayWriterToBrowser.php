<?php

namespace ArrayWriterToBrowser;

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level3/task3/Drivers/Writer.php';

use Writer\Writer as Writer;

class ArrayWriterToBrowser implements Writer
{
    public function write($massive)
    {
        echo implode($massive);
    }
}
