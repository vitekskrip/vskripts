<?php

namespace ConverterToUpper;

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level3/task3/Drivers/Converter.php';

use Converter\Converter as Converter;

class ConverterToUpper implements Converter
{
    public function convert($item)
    {
        return mb_strtoupper($item);
    }
}
