<?php

namespace ConverterToMenu;

include_once $_SERVER['DOCUMENT_ROOT'] . '/oop/level3/task3/Drivers/Converter.php';

use Converter\Converter as Converter;

class ConverterToMenu implements Converter
{
    public function convert($item)
    {
        return "<ul>
                    <li><a href='#'>$item</a></li>
                </ul>";
    }
}
