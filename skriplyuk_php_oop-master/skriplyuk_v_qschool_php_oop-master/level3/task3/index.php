<?php

include __DIR__ . '/Implementations/ArrayReader.php';
include __DIR__ . '/Implementations/ArrayWriterToBrowser.php';
include __DIR__ . '/Implementations/ConverterToUpper.php';
include __DIR__ . '/Implementations/ConvertToMenu.php';
include __DIR__ . '/Import/Import.php';

use Import\Import;
use ConverterToUpper\ConverterToUpper;
use ArrayReader\ArrayReader;
use ArrayWriterToBrowser\ArrayWriterToBrowser;
use ConverterToMenu\ConverterToMenu;

(new Import())
   ->from(new ArrayReader())
   ->to(new ArrayWriterToBrowser())
   ->with(new ConverterToUpper()) // преобразует в верхний регистр
   ->with(new ConverterToMenu()) // преобразует в пункты меню
   ->execute()
;
// в ArrayReader в качестве приватного свойства задан массив пунктов меню
