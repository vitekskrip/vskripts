<?php

namespace Writer;

interface Writer
{
    public function write($data);
}
