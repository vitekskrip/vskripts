<?php

namespace Converter;

interface Converter
{
    public function convert($item);
}
