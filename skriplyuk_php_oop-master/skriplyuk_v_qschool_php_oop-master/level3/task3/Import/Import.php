<?php

namespace Import;

use Reader\Reader as Reader;
use Writer\Writer as Writer;
use Converter\Converter as Converter;

class Import
{
    private Reader $reader;
    private Writer $writer;
    private $converters = [];

    public function from(Reader $reader)
    {
        $this->reader = $reader;
        return $this;
    }

    public function to(Writer $writer)
    {
        $this->writer = $writer;
        return $this;
    }

    public function with(Converter $converter)
    {
        $this->converters[] = $converter;
        return $this;
    }

    public function execute()
    {
        $massive = $this->reader->read();

        foreach ($this->converters as $converter) {
            foreach ($massive as $key => $element) {
                $massive[$key] =  $converter->convert($element);
            }
        }
        $this->writer->write($massive);
    }
}
