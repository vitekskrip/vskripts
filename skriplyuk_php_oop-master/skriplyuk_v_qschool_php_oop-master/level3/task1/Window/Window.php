<?php

namespace Window;

use Pillow\Pillow as Pillow;

class Window
{
    public $pillowsCount;

    public function tryHitMe(Pillow $pillow)
    {
        return rand(true, false) ? $this->pillowsCount++ : null;
    }
}
