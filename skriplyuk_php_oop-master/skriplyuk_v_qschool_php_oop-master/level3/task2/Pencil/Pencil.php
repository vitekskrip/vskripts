<?php

namespace Pencil;

class Pencil
{
    public $color;
    public $softness;
    public $price;

    public function __construct($color, $softness, $price)
    {
        $this->color = $color;
        $this->softness = $softness;
        $this->price = $price;
    }
}
