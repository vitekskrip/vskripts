<?php

include __DIR__ . '/PencilFactory/PencilFactory.php';

use PencilFactory\PencilFactory as PencilFactory;

$pencilFactory = new PencilFactory();
$pencilPack = [];
$pencilPack[] = $pencilFactory::createBlackFiftyRublesPencil('B');
$pencilPack[] = $pencilFactory::createBrownPencil('H', 150);
$pencilPack[] = $pencilFactory::createGreenOneHundredRublesPencil('HB');
$pencilPack[] = $pencilFactory::createOrangePencil('F', 100);
$pencilPack[] = $pencilFactory::createRedTwoHundredRublesPencil('B');
$pencilPack[] = $pencilFactory::createTwoHundredPencil('yellow', 'B');
var_dump($pencilPack);
