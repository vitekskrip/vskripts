<?php

namespace PencilFactory;

use Pencil\Pencil as Pencil;

class PencilFactory
{
    public static function createGreenOneHundredRublesPencil($softness)
    {
        return new Pencil('green', $softness, 100);
    }

    public static function createRedTwoHundredRublesPencil($softness)
    {
        return new Pencil('red', $softness, 200);
    }

    public static function createBlackFiftyRublesPencil($softness)
    {
        return new Pencil('black', $softness, 50);
    }

    public static function createOrangePencil($softness, $price)
    {
        return new Pencil('black', $softness, $price);
    }

    public static function createBrownPencil($softness, $price)
    {
        return new Pencil('brown', $softness, $price);
    }

    public static function createTwoHundredPencil($color, $softness)
    {
        return new Pencil($color, $softness, 200);
    }
}
