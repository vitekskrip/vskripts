<?php
$varStr = 'Непустая строка';
$varBool = true;
$varInt = 10;
$varFloat = 10.1;
echo $varStr . '</br>' . $varBool . '</br>' . $varInt . '</br>' . $varFloat . '</br>';

// $not valid
// $
// $3wa

$strFirst = 'operand1';
$strSecond = 'operator';
$strThird = 'operand2';
echo $strFirst . ' ' . $strSecond . ' ' . $strThird . '</br>';
echo $strThird . ' ' . $strSecond . ' ' . $strFirst . '</br>';
echo $strThird . ' ' . $strSecond . ' ' . $strFirst . ' = Ответ' . '</br>';
