<?php
$massive = [1, 2 ,3];
var_dump($massive[1]);
echo '</br>';
$massAssoc = ['key1' => 1, 'key2' => 2, 'key3' => 3];
//var_dump($massAssoc['key2']);
var_dump(array_slice($massAssoc, 1, 1, true));
echo '</br>';
$matrix = [
    [10, 20, 30],
    [40, 50, 60],
    [70, 80, 90],
];
var_dump($matrix[0][1] + $matrix[2][2] + $matrix[2][0]);
