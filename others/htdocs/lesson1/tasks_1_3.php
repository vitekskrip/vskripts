<?php
$result1 = ['author' => [
    'ФИО' => 'Pushkin',
    'email' => 'asd@gmail.com'
],
    'book' =>  [
        'Название' => 'Дубровский',
        'email' => 'asd@gmail.com'
    ],
];
//var_dump($result1);

$result2 = ['author' => [
    [
        'ФИО' => 'Pushkin',
        'email' => 'pushkin@gmail.com',
    ],
    [
        'ФИО' => 'Gogol',
        'email' => 'gogol@gmail.com',
    ]
],
    'book' => [
        [
            'Название' => 'Дубровский',
            'email' => 'pushkin@gmail.com',
        ],
        [
            'Название' => 'Мертвые души',
            'email' => 'gogol@gmail.com',
        ]
    ],
];

$library = ['author' => [
    'pushkin@gmail.com' => [
        'NAME' => 'Пушкин',
        'EMAIL' => 'pushkin@gmail.com',
        'YEAR' => 1799
    ],
    'gogol@gmail.com' => [
        'NAME' => 'Гоголь',
        'EMAIL' => 'gogol@gmail.com',
        'YEAR' => 1809
    ],
    'turgenev@gmail.com' => [
        'NAME' => 'Тургенев',
        'EMAIL' => 'turgenev@gmail.com',
        'YEAR' => 1818
    ]
],
    'book' => [
        [
            'NAME' => 'Дубровский',
            'EMAIL' => 'pushkin@gmail.com',
            'DATE' => 1841
        ],
        [
            'NAME' => 'Мертвые души',
            'EMAIL' => 'gogol@gmail.com',
            'DATE' => 1842
        ],
        [
            'NAME' => 'Отцы и дети',
            'EMAIL' => 'turgenev@gmail.com',
            'DATE' => 1862
        ],
        [
            'NAME' => 'Евгений Онегин',
            'EMAIL' => 'pushkin@gmail.com',
            'DATE' => 1833
        ]
    ],
];
var_dump($library['author'][$library['book'][1]['EMAIL']]['NAME']);
