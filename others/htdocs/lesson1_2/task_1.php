<?php
$library = ['author' => [
    'pushkin@gmail.com' => [
        'NAME' => 'Пушкин',
        'EMAIL' => 'pushkin@gmail.com',
        'YEAR' => 1799
    ],
    'gogol@gmail.com' => [
        'NAME' => 'Гоголь',
        'EMAIL' => 'gogol@gmail.com',
        'YEAR' => 1809
    ],
    'turgenev@gmail.com' => [
        'NAME' => 'Тургенев',
        'EMAIL' => 'turgenev@gmail.com',
        'YEAR' => 1818
    ]
],
    'book' => [
        [
            'NAME' => 'Дубровский',
            'EMAIL' => 'pushkin@gmail.com',
            'DATE' => 1841
        ],
        [
            'NAME' => 'Мертвые души',
            'EMAIL' => 'gogol@gmail.com',
            'DATE' => 1842
        ],
        [
            'NAME' => 'Отцы и дети',
            'EMAIL' => 'turgenev@gmail.com',
            'DATE' => 1862
        ],
        [
            'NAME' => 'Евгений Онегин',
            'EMAIL' => 'pushkin@gmail.com',
            'DATE' => 1833
        ]
    ],
];
//"Книга <Название книги>, ее написал <Фио автора> <Год Рождения автора> (<email автора>)"
foreach ($library['book'] as $book) {
    echo 'Книга ' . $book['NAME'] . ', ее написал ' . $library['author'][$book['EMAIL']]['NAME'] . ' ' . $library['author'][$book['EMAIL']]['YEAR'] . '(' . $book['EMAIL'] . ')' . '</br>';
}
//Перемешивание
shuffle($library['book']);
echo '</br>';
foreach ($library['book'] as $book) {
    echo 'Книга ' . $book['NAME'] . ', ее написал ' . $library['author'][$book['EMAIL']]['NAME'] . ' ' . $library['author'][$book['EMAIL']]['YEAR'] . '(' . $book['EMAIL'] . ')' . '</br>';
}
