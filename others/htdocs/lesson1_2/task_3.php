<?php
$foods = ['Яблоко', 'Клубника', 'Апельсин', 'Кабачок', 'Патиссон', 'Банан', 'Арбуз', 'Картошка', 'Лягушачие лапки'];
$foodItem = rand(0, 8);
switch ($foods[$foodItem]) {
    case 'Клубника':
    case 'Арбуз':
        $foodType = 'Ягода';
        break;
    case 'Яблоко':
    case 'Апельсин':
    case 'Банан':
        $foodType = 'Фрукт';
        break;
    case 'Кабачок':
    case 'Патиссон':
    case 'Картошка':
        $foodType = 'Овощ';
        break;
    default:
        $foodType = 'Что-то не вегетарианское';
        break;
}
echo 'Выбранный продукт ' . $foods[$foodItem] . ' - это ' . $foodType;
