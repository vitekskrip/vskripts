<?php
$library = ['author' => [
    'pushkin@gmail.com' => [
        'NAME' => 'Пушкин',
        'EMAIL' => 'pushkin@gmail.com',
        'YEAR' => 1799
    ],
    'gogol@gmail.com' => [
        'NAME' => 'Гоголь',
        'EMAIL' => 'gogol@gmail.com',
        'YEAR' => 1809
    ],
    'turgenev@gmail.com' => [
        'NAME' => 'Тургенев',
        'EMAIL' => 'turgenev@gmail.com',
        'YEAR' => 1818
    ]
],
    'book' => [
        [
            'NAME' => 'Дубровский',
            'EMAIL' => 'pushkin@gmail.com',
            'DATE' => 1841
        ],
        [
            'NAME' => 'Мертвые души',
            'EMAIL' => 'gogol@gmail.com',
            'DATE' => 1842
        ],
        [
            'NAME' => 'Отцы и дети',
            'EMAIL' => 'turgenev@gmail.com',
            'DATE' => 1862
        ],
        [
            'NAME' => 'Евгений Онегин',
            'EMAIL' => 'pushkin@gmail.com',
            'DATE' => 1833
        ]
    ],
];
$title = 'Библиотека';
$red = (bool)rand(0, 1);
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$title;?></title>
    <style type="text/css">.red {color: red;}</style>
</head>
<body>
<h1 <?=$red ? 'class="red"' : ''?>><?=$title?></h1>
<!-- Выведите реально количество авторов -->
<div>Всего авторов на портале <?=count($library['author'])?></div>

<!-- Выведите все книги -->
<?php foreach ($library['book'] as $book) : ?>
<p>Книга <?=$book['NAME']?>, ее написал <?=$library['author'][$book['EMAIL']]['NAME'] . ' ' . $library['author'][$book['EMAIL']]['YEAR'] . ' (' . $book['EMAIL'] . ')' . '</br>'?></p>
<?php endforeach;?>
</body>
</html>
