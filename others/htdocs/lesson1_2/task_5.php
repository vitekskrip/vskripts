<?php
$studentsCount = rand(1, 1000000);
if ($studentsCount % 100 < 11 || $studentsCount % 100 > 14) {
    $lastDigit = $studentsCount % 10;
    if ($lastDigit == 1) {
        echo "на учебе $studentsCount студент";
    } elseif ($lastDigit >= 2 && $lastDigit <= 4) {
        echo "на учебе $studentsCount студента";
    } else {
        echo "на учебе $studentsCount студентов";
    } 
} else {
    echo "на учебе $studentsCount студентов";
}