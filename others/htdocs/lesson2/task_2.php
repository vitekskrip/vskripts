<?php

$menu = [
    [
        'title' => 'Главная страница',
        'sort' => 1,
        'path' => '/',
    ],
    [
        'title' => 'О нас',
        'sort' => 110,
        'path' => '/about',
    ],
    [
        'title' => 'О QSOFT',
        'sort' => 10,
        'path' => '/qsoft',
    ],
    [
        'title' => 'О Skillbox',
        'sort' => 9,
        'path' => '/skillbox',
    ],
    [
        'title' => 'О погоде',
        'sort' => 9200,
        'path' => '/weather',
    ],
];

function arraySort(array $array, $key = 'sort', $sort = SORT_ASC): array
{
    usort($array, function ($item1, $item2) use ($key, $sort) {
        return $sort === SORT_ASC ? $item1[$key] <=> $item2[$key] : $item2[$key] <=> $item1[$key];
    });
    return $array;
}

var_dump(arraySort($menu));
/**
 * Должен получиться следующий порядок:
 * - Главная страница
 * - О Skillbox
 * - О QSOFT
 * - О нас
 * - О погоде
 */
