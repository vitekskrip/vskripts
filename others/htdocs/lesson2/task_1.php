<?php

function cutString($line, $length = 12, $appends = '...'): string
{
    if (strlen($line) > $length) {
        $line = substr($line, 0, $length) . $appends;
    }
    return $line;
}

$lines = ['vitek', 'vitekvitekvitekvitekvitek', 'kaasdfxzczxdasd', 'asd9asduu90sad90sadasd', 'qwertyuiopasd', 'dzdgbhgsdf', 'three'];
foreach ($lines as $line) {
    $finalMass[] = cutString($line);
}
var_dump($finalMass);
